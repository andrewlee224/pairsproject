import sys
import Bio
import rnaPairs


def createPairsFile(filename, outfilename):
    f = open(filename)
    pairs = f.read()
    rnaList = [ el.strip() for el in pairs.split("_mcannotate") ]
    p = Bio.PDB.PDBParser()
    toAppend = "_mcannotate.pdb"
    fDir = "files//"

    fw = open(outfilename, 'w')
    for i in range(len(rnaList)):
      fw.write(rnaList[i])
      if (i+1) % 2 == 0:
        # count pair no. of atoms
        s1 = p.get_structure(rnaList[i-1], fDir + rnaList[i-1] + toAppend)
        s2 = p.get_structure(rnaList[i], fDir + rnaList[i] + toAppend)
        count = rnaPairs.getAtomCount(s1) + rnaPairs.getAtomCount(s2)
        fw.write(" " + str(count))
        fw.write("\n")
      else:
        fw.write(" ")

def createPairsCountFile(filename, outfilename):
    f = open(filename)

if __name__ == '__main__':
    filename = sys.argv[1]
    outfilename = sys.argv[2]

    createPairsFile(filename, outfilename)
